package training.users.dao;

import java.sql.Connection;

public class UsersDaoFactory {

    public UsersDAO createUsersDAO(Connection connection) {
        return new UsersDAO(connection);
    }
}
