package training.users.dao;

import training.users.User;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class UsersDAO {

    private final Connection conn;

    public UsersDAO(Connection connection) {
        this.conn = connection;
    }

    public User getUser(String username, String pass) throws SQLException {
        String query = "SELECT * FROM users " +
                "WHERE user_name = ? AND password = ?";

        try (PreparedStatement selectStatement = conn.prepareStatement(query)) {
            selectStatement.setString(1, username);
            selectStatement.setString(2, pass);
            try (ResultSet rs = selectStatement.executeQuery()) {
                if (rs.next())
                    return getUserInfo(rs);
                return null;
            }
        }
    }

    public int createUser(User user, String password) throws SQLException {
        String query = "INSERT INTO users (user_name, password, first_name, last_name, is_manager) " +
                "VALUES (?,?,?,?,?)";
        try (PreparedStatement insertSt = conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS)) {

            insertSt.setString(1, user.getUsername());
            insertSt.setString(2, password);
            insertSt.setString(3, user.getFirstName());
            insertSt.setString(4, user.getLastName());
            insertSt.setBoolean(5, user.getIsManager());
            insertSt.executeUpdate();

            try (ResultSet generatedKeys = insertSt.getGeneratedKeys()) {
                if (generatedKeys.next())
                    return generatedKeys.getInt(1);
                throw new SQLException("Failed to create user, no ID obtained");
            }
        }
    }

    public void updateUser(User user) throws SQLException {
        String query = "UPDATE users SET user_name = ?, first_name = ?, last_name = ?, is_manager = ? " +
                "WHERE id = ?";
        try (PreparedStatement updateSt = conn.prepareStatement(query)) {

            updateSt.setString(1, user.getUsername());
            updateSt.setString(2, user.getFirstName());
            updateSt.setString(3, user.getLastName());
            updateSt.setBoolean(4, user.getIsManager());
            updateSt.setInt(5, user.getId());

            updateSt.executeUpdate();
        }
    }

    public void updatePassword(int userId, String password) throws SQLException {
        String query = "UPDATE users SET password = ? " +
                "WHERE id = ?";
        try (PreparedStatement updateSt = conn.prepareStatement(query)) {
            updateSt.setString(1, password);
            updateSt.setInt(2, userId);

            updateSt.executeUpdate();
        }
    }

    public void deleteUser(int userId) throws SQLException {
        String query = "DELETE FROM users WHERE id = ?";
        try (PreparedStatement deleteSt = conn.prepareStatement(query)) {

            deleteSt.setInt(1, userId);
            deleteSt.executeUpdate();
        }
    }

    public User getUser(String username) throws SQLException {
        String query = "SELECT id, user_name, first_name, last_name, is_manager " +
                "FROM users WHERE user_name = ?";
        try (PreparedStatement selectSt = conn.prepareStatement(query)) {
            selectSt.setString(1, username);
            try (ResultSet rs = selectSt.executeQuery()) {
                if (rs.next())
                    return getUserInfo(rs);
            }
        }
        return null;
    }

    public User getUser(int id) throws SQLException {
        String query = "SELECT id, user_name, first_name, last_name, is_manager " +
                "FROM users WHERE id = ?";
        try (PreparedStatement selectSt = conn.prepareStatement(query)) {
            selectSt.setInt(1, id);
            try (ResultSet rs = selectSt.executeQuery()) {
                if (rs.next())
                    return getUserInfo(rs);
            }
        }
        return null;
    }

    public User[] getUsersList() throws SQLException {
        String query = "SELECT id, user_name, first_name, last_name, is_manager FROM users";
        try (PreparedStatement selectSt = conn.prepareStatement(query)) {
            try (ResultSet usersSet = selectSt.executeQuery()) {

                List<User> users = new ArrayList<>();
                while (usersSet.next()) {
                    users.add(getUserInfo(usersSet));
                }
                return users.toArray(new User[users.size()]);
            }
        }
    }

    private User getUserInfo(ResultSet rs) throws SQLException {
        return new User(
                rs.getInt("id"),
                rs.getString("user_name"),
                rs.getString("first_name"),
                rs.getString("last_name"),
                rs.getBoolean("is_manager"));
    }
}
