package training.users.servlets;

import training.users.User;
import training.users.dao.UsersDAO;
import training.users.utils.AttributesNames;
import training.users.utils.PageNames;
import training.users.utils.RequestUtils;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

public class LoginServlet extends HttpServlet {

    public static final String LOGIN_PARAMETER = "login";
    public static final String PASSWORD_PARAMETER = "pass";


    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (RequestUtils.isUserLoggedIn(req)) {
            resp.sendRedirect(PageNames.USERS_LIST_PAGE);
            return;
        }
        RequestDispatcher rs = req.getRequestDispatcher(PageNames.LOGIN_PAGE);
        rs.forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String login = req.getParameter(LOGIN_PARAMETER);
        String pass = req.getParameter(PASSWORD_PARAMETER);

        if (login == null)
            throw new ServletException("Request doesn't contain login parameter");
        if (pass == null)
            throw new ServletException("Request doesn't contain password parameter");

        UsersDAO usersDAO = RequestUtils.getUsersDAO(req);
        try {
            User user = usersDAO.getUser(login, pass);
            if (user == null) {
                req.setAttribute(AttributesNames.ERROR_MSG, "Username or password incorrect");
                RequestDispatcher rs = req.getRequestDispatcher(PageNames.LOGIN_PAGE);
                rs.forward(req, resp);
            } else {
                createLoginSession(req, user.getId());
                resp.sendRedirect(PageNames.USERS_LIST_PAGE);
            }
        } catch (SQLException e) {
            throw new ServletException(e);
        }
    }

    private void createLoginSession(HttpServletRequest req, int userID) {
        req.getSession(true).setAttribute(AttributesNames.USER_ID, userID);
    }

}
