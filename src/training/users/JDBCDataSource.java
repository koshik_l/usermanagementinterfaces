package training.users;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

public class JDBCDataSource implements AutoCloseable {

    private static final String ENV_CONTEXT = "java:comp/env";
    private static final String JDBC_CONTEXT = "jdbc/UsersDB";

    private Context initContext;
    private DataSource dataSource;

    public JDBCDataSource() throws NamingException {
        initContext = new InitialContext();
        Context envContext = (Context) initContext.lookup(ENV_CONTEXT);
        dataSource = (DataSource) envContext.lookup(JDBC_CONTEXT);
    }

    public Connection getConnection() throws SQLException {
        return dataSource.getConnection();
    }

    @Override
    public void close() throws Exception {
        initContext.close();
    }
}
