package training.users.utils;

import training.users.JDBCDataSource;
import training.users.dao.UsersDAO;
import training.users.dao.UsersDaoFactory;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.sql.Connection;

public class RequestUtils {

    public static Connection getConnection(HttpServletRequest req) {
        return (Connection) req.getAttribute(AttributesNames.CONNECTION);
    }

    public static UsersDAO getUsersDAO(HttpServletRequest req) {
        UsersDaoFactory daoFactory = ((UsersDaoFactory) req.getServletContext().getAttribute(AttributesNames.DAO_FACTORY));
        Connection connection = getConnection(req);
        return daoFactory.createUsersDAO(connection);
    }

    public static JDBCDataSource getJDBCDataSource(ServletRequest req) {
        return (JDBCDataSource) req.getServletContext().getAttribute(AttributesNames.JDBC_DATA_SOURCE);
    }

    public static int getLoggedInUserId(HttpServletRequest req) {
        return (int) req.getSession(false).getAttribute(AttributesNames.USER_ID);
    }

    public static boolean isUserLoggedIn(HttpServletRequest req) {
        HttpSession session = req.getSession(false);
        return session != null && session.getAttribute(AttributesNames.USER_ID) != null;
    }
}
