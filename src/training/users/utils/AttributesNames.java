package training.users.utils;

public class AttributesNames {
    public static final String USER_ID = "userID";
    public static final String ERROR_MSG = "errorMsg";
    public static final String IS_MANAGER = "isManager";
    public static final String CONNECTION = "usersDbConnection";
    public static final String DAO_FACTORY = "usersDaoFactory";
    public static final String JDBC_DATA_SOURCE = "jdbc";
}
