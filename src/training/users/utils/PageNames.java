package training.users.utils;

public class PageNames {

    public static final String LOGIN_PAGE = "login.jsp";
    public static final String LOGIN_SERVLET = "login";
    public static final String USERS_LIST_PAGE = "users-list";

}
