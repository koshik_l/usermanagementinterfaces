package training.users;

import java.io.Serializable;
import java.util.Objects;

public class User implements Serializable {

    private String username;
    private String firstName;
    private String lastName;
    private boolean manager;
    private int id;

    public User() {
    }

    public User(int id, String username, String firstName, String lastName, boolean manager) {
        this.id = id;
        this.username = username;
        this.firstName = firstName;
        this.lastName = lastName;
        this.manager = manager;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public boolean getIsManager() {
        return manager;
    }

    public void setIsManager(boolean manager) {
        this.manager = manager;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return id == user.id &&
                Objects.equals(username, user.username) &&
                Objects.equals(firstName, user.firstName) &&
                Objects.equals(lastName, user.lastName) &&
                Objects.equals(manager, user.manager);
    }

    @Override
    public int hashCode() {

        return Objects.hash(username, firstName, lastName, manager, id);
    }
}
