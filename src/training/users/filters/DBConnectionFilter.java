package training.users.filters;

import training.users.utils.AttributesNames;
import training.users.utils.RequestUtils;

import javax.servlet.*;
import java.sql.Connection;
import java.sql.SQLException;

public class DBConnectionFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) {
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
            throws ServletException {

        Connection conn = null;
        try {
            conn = RequestUtils.getJDBCDataSource(servletRequest).getConnection();
            conn.setAutoCommit(false);
            servletRequest.setAttribute(AttributesNames.CONNECTION, conn);

            filterChain.doFilter(servletRequest, servletResponse);
            conn.commit();
        } catch (Exception e) {
            if (conn != null)
                rollback(conn);
            throw new ServletException(e);
        } finally {
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException ignored) {
            }
        }
    }

    private void rollback(Connection connection) throws ServletException {
        try {
            connection.rollback();
        } catch (SQLException e1) {
            throw new ServletException(e1);
        }
    }

    @Override
    public void destroy() {
    }
}
