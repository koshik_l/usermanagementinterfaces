package training.users.filters;

import training.users.User;
import training.users.dao.UsersDAO;
import training.users.utils.AttributesNames;
import training.users.utils.PageNames;
import training.users.utils.RequestUtils;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

public class LoginFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
            throws IOException, ServletException {
        if (servletRequest instanceof HttpServletRequest) {
            HttpServletRequest req = (HttpServletRequest) servletRequest;
            if (!RequestUtils.isUserLoggedIn(req)) {
                ((HttpServletResponse) servletResponse).sendRedirect(PageNames.LOGIN_SERVLET);
                return;
            }

            try {
                User loggedInUser = getUser(req);
                req.setAttribute(AttributesNames.IS_MANAGER, loggedInUser.getIsManager());
            } catch (SQLException e) {
                throw new ServletException(e);
            }
        }
        filterChain.doFilter(servletRequest, servletResponse);
    }

    private User getUser(HttpServletRequest request) throws SQLException {
        UsersDAO usersDAO = RequestUtils.getUsersDAO(request);
        int loggedInUserId = RequestUtils.getLoggedInUserId(request);
        return usersDAO.getUser(loggedInUserId);
    }

    @Override
    public void destroy() {

    }
}
