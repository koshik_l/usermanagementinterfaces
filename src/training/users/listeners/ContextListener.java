package training.users.listeners;

import org.jabsorb.JSONRPCBridge;
import training.users.JDBCDataSource;
import training.users.dao.UsersDaoFactory;
import training.users.service.UsersService;
import training.users.service.callbacks.UserAccessChecker;
import training.users.service.callbacks.UsersServiceErrorCallback;
import training.users.utils.AttributesNames;

import javax.naming.NamingException;
import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.http.HttpServletRequest;

public class ContextListener implements ServletContextListener {

    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        ServletContext context = servletContextEvent.getServletContext();

        context.setAttribute(AttributesNames.DAO_FACTORY, new UsersDaoFactory());
        initJSONRPCBridge();


        try {
            context.setAttribute(AttributesNames.JDBC_DATA_SOURCE, new JDBCDataSource());
        } catch (NamingException e) {
            e.printStackTrace();
        }

    }

    private void initJSONRPCBridge() {
        JSONRPCBridge bridge = JSONRPCBridge.getGlobalBridge();
        UsersService usersService = new UsersService();
        bridge.registerObject("usersService", usersService);
        bridge.registerCallback(new UserAccessChecker(), HttpServletRequest.class);
        bridge.registerCallback(new UsersServiceErrorCallback(), HttpServletRequest.class);
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {
        JDBCDataSource dataSource = (JDBCDataSource) servletContextEvent.getServletContext().
                getAttribute(AttributesNames.JDBC_DATA_SOURCE);
        if (dataSource != null) {
            try {
                dataSource.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
