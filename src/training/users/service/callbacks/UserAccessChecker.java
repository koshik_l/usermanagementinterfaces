package training.users.service.callbacks;

import org.jabsorb.callback.InvocationCallback;
import training.users.User;
import training.users.dao.UsersDAO;
import training.users.service.AccessException;
import training.users.service.RequirePermission;
import training.users.utils.RequestUtils;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.AccessibleObject;
import java.lang.reflect.Method;

public class UserAccessChecker implements InvocationCallback {

    @Override
    public void preInvoke(Object context, Object instance, AccessibleObject accessibleObject, Object[] argument)
            throws Exception {
        HttpServletRequest req = (HttpServletRequest) context;

        int id = getLoggedInUserId(req);

        Method method = (Method) accessibleObject;
        if (isAnnotated(method)) {
            UsersDAO usersDAO = RequestUtils.getUsersDAO(req);
            User user = usersDAO.getUser(id);
            if (!user.getIsManager())
                throw new AccessException("Not allowed to call method " + method.getName());
        }
    }

    private int getLoggedInUserId(HttpServletRequest req) throws AccessException {
        if (!RequestUtils.isUserLoggedIn(req))
            throw new AccessException("Session doesn't exist");
        return RequestUtils.getLoggedInUserId(req);
    }

    private boolean isAnnotated(Method method) {
        return method.getAnnotation(RequirePermission.class) != null;
    }

    @Override
    public void postInvoke(Object o, Object o1, AccessibleObject accessibleObject, Object o2) {
    }
}
