package training.users.service.callbacks;

import org.jabsorb.callback.ErrorInvocationCallback;
import training.users.utils.RequestUtils;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.AccessibleObject;
import java.sql.Connection;
import java.sql.SQLException;

public class UsersServiceErrorCallback implements ErrorInvocationCallback {
    @Override
    public void invocationError(Object context, Object instance, AccessibleObject accessibleObject, Throwable throwable) {
        if (context instanceof HttpServletRequest) {
            HttpServletRequest req = (HttpServletRequest) context;
            Connection connection = RequestUtils.getConnection(req);
            try {
                connection.rollback();
            } catch (SQLException ignored) {
            }
        }
    }

    @Override
    public void preInvoke(Object o, Object o1, AccessibleObject accessibleObject, Object[] objects) {

    }

    @Override
    public void postInvoke(Object o, Object o1, AccessibleObject accessibleObject, Object o2) {
    }
}
