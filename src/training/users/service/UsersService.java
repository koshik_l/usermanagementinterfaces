package training.users.service;

import training.users.User;
import training.users.dao.UsersDAO;
import training.users.utils.RequestUtils;

import javax.servlet.http.HttpServletRequest;
import java.sql.SQLException;

public class UsersService {

    public User[] getUsersList(HttpServletRequest req) throws SQLException {
        UsersDAO usersDAO = RequestUtils.getUsersDAO(req);

        return usersDAO.getUsersList();
    }

    @RequirePermission
    public int newUser(User user, String password, HttpServletRequest req)
            throws UserModificationException, SQLException {
        UsersDAO usersDAO = RequestUtils.getUsersDAO(req);

        if (usersDAO.getUser(user.getUsername()) != null)
            throw new UserModificationException("User with login " + user.getUsername() + " already exists");

        return usersDAO.createUser(user, password);

    }

    @RequirePermission
    public void deleteUser(int userId, HttpServletRequest req) throws UserModificationException, SQLException, AccessException {
        UsersDAO usersDAO = RequestUtils.getUsersDAO(req);

        int curUserId = RequestUtils.getLoggedInUserId(req);
        if (curUserId == userId)
            throw new UserModificationException("User can not delete himself");

        usersDAO.deleteUser(userId);
    }

    @RequirePermission
    public void updateUser(User newUser, String newPassword, HttpServletRequest req)
            throws UserModificationException, SQLException, AccessException {
        UsersDAO usersDAO = RequestUtils.getUsersDAO(req);

        User user = usersDAO.getUser(newUser.getId());
        if (user == null)
            throw new UserModificationException("User doesn't exist");

        if (!user.getUsername().equals(newUser.getUsername()) &&
                usersDAO.getUser(newUser.getUsername()) != null)
            throw new UserModificationException("User with login " + newUser.getUsername() + " already exists");

        int curUserId = RequestUtils.getLoggedInUserId(req);
        if (user.getId() == curUserId && !newUser.getIsManager())
            throw new UserModificationException("Manager cannot change his manager state");

        if (!"".equals(newPassword))
            usersDAO.updatePassword(user.getId(), newPassword);
        usersDAO.updateUser(newUser);
    }
}
