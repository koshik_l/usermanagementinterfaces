package training.users.servlets;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.Mockito;
import training.users.User;
import training.users.dao.UsersDAO;
import training.users.dao.UsersDaoFactory;
import training.users.utils.AttributesNames;
import training.users.utils.PageNames;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import static org.hamcrest.core.Is.is;
import static org.mockito.Mockito.*;

public class LoginServletTest {

    private UsersDAO usersDAO;
    private HttpServletRequest req;
    private HttpServletResponse resp;

    @Before
    public void setUp() {
        req = mock(HttpServletRequest.class);
        resp = mock(HttpServletResponse.class);

        usersDAO = mock(UsersDAO.class);

        ServletContext context = mock(ServletContext.class);
        UsersDaoFactory daoFactory = mock(UsersDaoFactory.class);
        when(daoFactory.createUsersDAO(Mockito.any(Connection.class))).thenReturn(usersDAO);
        when(context.getAttribute(AttributesNames.DAO_FACTORY)).thenReturn(daoFactory);
        when(req.getServletContext()).thenReturn(context);
    }

    @Test
    public void doGetLoggedInUser() throws ServletException, IOException {
        HttpSession session = mock(HttpSession.class);
        when(req.getSession(false)).thenReturn(session);
        int id = 0;
        when(session.getAttribute(AttributesNames.USER_ID)).thenReturn(id);

        LoginServlet loginServlet = new LoginServlet();
        loginServlet.doGet(req, resp);

        verify(resp).sendRedirect(PageNames.USERS_LIST_PAGE);
    }

    @Test
    public void doGetNullSession() throws ServletException, IOException {
        HttpSession session = mock(HttpSession.class);
        when(req.getSession(false)).thenReturn(null);
        RequestDispatcher rs = mock(RequestDispatcher.class);
        when(req.getRequestDispatcher(PageNames.LOGIN_PAGE)).thenReturn(rs);

        LoginServlet loginServlet = new LoginServlet();
        loginServlet.doGet(req, resp);

        verify(resp, (never())).sendRedirect(PageNames.USERS_LIST_PAGE);
        verify(rs).forward(req, resp);
    }

    @Test
    public void doGetNullUserId() throws ServletException, IOException {
        HttpSession session = mock(HttpSession.class);
        when(req.getSession(false)).thenReturn(session);
        when(session.getAttribute(AttributesNames.USER_ID)).thenReturn(null);
        RequestDispatcher rs = mock(RequestDispatcher.class);
        when(req.getRequestDispatcher(PageNames.LOGIN_PAGE)).thenReturn(rs);

        LoginServlet loginServlet = new LoginServlet();
        loginServlet.doGet(req, resp);

        verify(resp, (never())).sendRedirect(PageNames.USERS_LIST_PAGE);
        verify(rs).forward(req, resp);
    }

    @Test
    public void doPostUserExists() throws SQLException, ServletException, IOException {
        String login = "login";
        String pass = "pass";
        when(req.getParameter(LoginServlet.LOGIN_PARAMETER)).thenReturn(login);
        when(req.getParameter(LoginServlet.PASSWORD_PARAMETER)).thenReturn(pass);

        int id = 0;
        User user = new User(id, login, "firstName", "lastName", false);
        when(usersDAO.getUser(login, pass)).thenReturn(user);

        HttpSession session = mock(HttpSession.class);
        when(req.getSession(true)).thenReturn(session);

        LoginServlet loginServlet = new LoginServlet();
        loginServlet.doPost(req, resp);

        verify(session).setAttribute(eq(AttributesNames.USER_ID), Mockito.anyString());
        verify(resp).sendRedirect(PageNames.USERS_LIST_PAGE);
    }

    @Test
    public void doPostUserDoesNotExist() throws SQLException, ServletException, IOException {
        String login = "login";
        String pass = "pass";
        when(req.getParameter(LoginServlet.LOGIN_PARAMETER)).thenReturn(login);
        when(req.getParameter(LoginServlet.PASSWORD_PARAMETER)).thenReturn(pass);

        when(usersDAO.getUser(login, pass)).thenReturn(null);
        RequestDispatcher rs = mock(RequestDispatcher.class);
        when(req.getRequestDispatcher(PageNames.LOGIN_PAGE)).thenReturn(rs);

        LoginServlet loginServlet = new LoginServlet();
        loginServlet.doPost(req, resp);

        verify(req).setAttribute(eq(AttributesNames.ERROR_MSG), Mockito.anyString());
        verify(rs).forward(req, resp);
    }

    @Test(expected = ServletException.class)
    public void doPostNoLoginParameter() throws ServletException, IOException {
        String pass = "pass";
        when(req.getParameter(LoginServlet.LOGIN_PARAMETER)).thenReturn(null);
        when(req.getParameter(LoginServlet.PASSWORD_PARAMETER)).thenReturn(pass);

        LoginServlet loginServlet = new LoginServlet();
        loginServlet.doPost(req, resp);
    }

    @Test(expected = ServletException.class)
    public void doPostNoPassParameter() throws ServletException, IOException {
        String login = "login";
        when(req.getParameter(LoginServlet.LOGIN_PARAMETER)).thenReturn(login);
        when(req.getParameter(LoginServlet.PASSWORD_PARAMETER)).thenReturn(null);

        LoginServlet loginServlet = new LoginServlet();
        loginServlet.doPost(req, resp);
    }

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void doPostDaoThrowsSQlException() throws SQLException, ServletException, IOException {
        String login = "login";
        String pass = "pass";
        when(req.getParameter(LoginServlet.LOGIN_PARAMETER)).thenReturn(login);
        when(req.getParameter(LoginServlet.PASSWORD_PARAMETER)).thenReturn(pass);

        SQLException sqlException = new SQLException();
        doThrow(sqlException).when(usersDAO).getUser(login, pass);

        thrown.expect(ServletException.class);
        thrown.expectCause(is(sqlException));

        LoginServlet loginServlet = new LoginServlet();
        loginServlet.doPost(req, resp);
    }

}