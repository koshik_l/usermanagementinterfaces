package training.users.servlets;

import org.junit.Before;
import org.junit.Test;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

import static org.mockito.Mockito.*;

public class LogoutServletTest {

    private HttpServletRequest req;
    private HttpServletResponse resp;

    @Before
    public void setUp() {
        req = mock(HttpServletRequest.class);
        resp = mock(HttpServletResponse.class);
    }

    @Test
    public void doGetSessionExists() throws IOException {
        HttpSession session = mock(HttpSession.class);
        when(req.getSession(false)).thenReturn(session);
        String redirectPage = "redirectPage";
        when(req.getContextPath()).thenReturn(redirectPage);

        LogoutServlet servlet = new LogoutServlet();
        servlet.doGet(req, resp);

        verify(session).invalidate();
        verify(resp).sendRedirect(redirectPage);
    }

    @Test
    public void doGetSessionDoesNotExist() throws IOException {
        when(req.getSession(false)).thenReturn(null);
        String redirectPage = "redirectPage";
        when(req.getContextPath()).thenReturn(redirectPage);

        LogoutServlet servlet = new LogoutServlet();
        servlet.doGet(req, resp);

        verify(resp).sendRedirect(redirectPage);
    }
}