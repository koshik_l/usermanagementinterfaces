package training.users.service;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import training.users.User;
import training.users.dao.UsersDAO;
import training.users.dao.UsersDaoFactory;
import training.users.utils.AttributesNames;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.sql.Connection;
import java.sql.SQLException;

import static org.mockito.Mockito.*;

public class UsersServiceTest {

    private HttpServletRequest req;
    private UsersDAO usersDAO;
    private UsersService usersService;

    @Before
    public void setUp() {
        req = mock(HttpServletRequest.class);

        usersDAO = mock(UsersDAO.class);
        ServletContext context = mock(ServletContext.class);
        UsersDaoFactory daoFactory = mock(UsersDaoFactory.class);
        when(daoFactory.createUsersDAO(Mockito.any(Connection.class))).thenReturn(usersDAO);
        when(context.getAttribute(AttributesNames.DAO_FACTORY)).thenReturn(daoFactory);
        when(req.getServletContext()).thenReturn(context);

        usersService = new UsersService();
    }

    @Test
    public void newUserWithNotExistingLogin() throws SQLException, UserModificationException {
        String username = "username";
        when(usersDAO.getUser(username)).thenReturn(null);

        User user1 = new User(0, username, "", "", true);
        usersService.newUser(user1, "password", req);

        verify(usersDAO).createUser(user1, "password");
    }

    @Test(expected = UserModificationException.class)
    public void newUserWithExistingLogin() throws SQLException, UserModificationException {
        User user = new User(0, "username", "firstName", "lastName", false);
        when(usersDAO.getUser(user.getUsername())).thenReturn(user);

        User user1 = new User(1, "username", "", "", true);
        usersService.newUser(user1, "password", req);
    }

    @Test
    public void deleteUserWithIdNotEqualCurSessionId() throws UserModificationException, SQLException, AccessException {
        HttpSession session = mock(HttpSession.class);
        int id = 0;
        when(session.getAttribute(AttributesNames.USER_ID)).thenReturn(id);
        when(req.getSession(false)).thenReturn(session);

        usersService.deleteUser(1, req);

        verify(usersDAO).deleteUser(1);
    }

    @Test(expected = UserModificationException.class)
    public void deleteUserIdEqualsCurSessionId() throws UserModificationException, SQLException, AccessException {
        HttpSession session = mock(HttpSession.class);
        int id = 0;
        when(session.getAttribute(AttributesNames.USER_ID)).thenReturn(id);
        when(req.getSession(false)).thenReturn(session);

        usersService.deleteUser(id, req);
    }

    @Test
    public void updateUserChangeUserName() throws SQLException, UserModificationException, AccessException {
        String newLogin = "newUsername";
        User newUser = new User(0, newLogin, "", "", false);
        User dbUser = new User(0, "1", "1", "1", true);
        when(usersDAO.getUser(newUser.getId())).thenReturn(dbUser);
        when(usersDAO.getUser(newUser.getUsername())).thenReturn(null);

        HttpSession session = mock(HttpSession.class);
        when(session.getAttribute(AttributesNames.USER_ID)).thenReturn(1);
        when(req.getSession(false)).thenReturn(session);

        usersService.updateUser(newUser, "", req);

        verify(usersDAO).updateUser(newUser);
    }

    @Test
    public void updateUserManagerChangeHimselfExceptManagerState() throws SQLException, UserModificationException, AccessException {
        int id = 0;
        User newUser = new User(id, "1", "1", "1", true);
        User dbUser = new User(id, "", "", "", true);
        when(usersDAO.getUser(newUser.getId())).thenReturn(dbUser);

        HttpSession session = mock(HttpSession.class);
        when(session.getAttribute(AttributesNames.USER_ID)).thenReturn(id);
        when(req.getSession(false)).thenReturn(session);

        usersService.updateUser(newUser, "", req);
    }

    @Test(expected = UserModificationException.class)
    public void updateUserNotExistingUser() throws SQLException, UserModificationException, AccessException {
        User newUser = new User(0, "", "", "", true);
        when(usersDAO.getUser(newUser.getId())).thenReturn(null);

        usersService.updateUser(newUser, "password", req);
    }

    @Test(expected = UserModificationException.class)
    public void updateUserChangeLoginToExistingLogin() throws SQLException, UserModificationException, AccessException {
        String newLogin = "newUsername";
        User newUser = new User(0, newLogin, "", "", false);
        User dbUser = new User(0, "1", "1", "1", true);
        when(usersDAO.getUser(newUser.getId())).thenReturn(dbUser);
        when(usersDAO.getUser(newUser.getUsername())).
                thenReturn(new User(1, newLogin, "", "", false));

        usersService.updateUser(newUser, "password", req);

        verify(usersDAO, never()).updateUser(newUser);
    }

    @Test(expected = UserModificationException.class)
    public void updateUserManagerChangeHisManagerState() throws SQLException, UserModificationException, AccessException {
        int id = 0;
        User newUser = new User(id, "", "", "", false);
        User dbUser = new User(id, "", "", "", true);
        when(usersDAO.getUser(newUser.getId())).thenReturn(dbUser);

        HttpSession session = mock(HttpSession.class);
        when(session.getAttribute(AttributesNames.USER_ID)).thenReturn(id);
        when(req.getSession(false)).thenReturn(session);

        usersService.updateUser(newUser, "password", req);

        verify(usersDAO, never()).updateUser(newUser);
    }

    @Test
    public void updateUserUpdatePassword() throws SQLException, UserModificationException, AccessException {
        User newUser = new User(0, "", "", "", false);
        String newPassword = "newPassword";
        when(usersDAO.getUser(newUser.getId())).thenReturn(newUser);

        HttpSession session = mock(HttpSession.class);
        when(session.getAttribute(AttributesNames.USER_ID)).thenReturn(1);
        when(req.getSession(false)).thenReturn(session);

        usersService.updateUser(newUser, newPassword, req);

        verify(usersDAO).updatePassword(newUser.getId(), newPassword);
    }

    @Test
    public void updateUserEmptyPassword() throws SQLException, UserModificationException, AccessException {
        User newUser = new User(0, "", "", "", false);
        String newPassword = "";
        when(usersDAO.getUser(newUser.getId())).thenReturn(newUser);

        HttpSession session = mock(HttpSession.class);
        when(session.getAttribute(AttributesNames.USER_ID)).thenReturn(1);
        when(req.getSession(false)).thenReturn(session);

        usersService.updateUser(newUser, newPassword, req);

        verify(usersDAO, never()).updatePassword(newUser.getId(), newPassword);
    }

}