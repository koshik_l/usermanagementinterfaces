package training.users.service.callbacks;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import training.users.User;
import training.users.dao.UsersDAO;
import training.users.dao.UsersDaoFactory;
import training.users.service.AccessException;
import training.users.service.RequirePermission;
import training.users.utils.AttributesNames;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.lang.reflect.Method;
import java.sql.Connection;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class UserAccessCheckerTest {

    private HttpSession session;
    private HttpServletRequest req;
    private UsersDAO usersDAO;
    private UserAccessChecker userAccessChecker;

    @Before
    public void setUp() {
        req = mock(HttpServletRequest.class);

        session = mock(HttpSession.class);

        ServletContext context = mock(ServletContext.class);
        usersDAO = mock(UsersDAO.class);
        UsersDaoFactory daoFactory = mock(UsersDaoFactory.class);
        when(daoFactory.createUsersDAO(Mockito.any(Connection.class))).thenReturn(usersDAO);
        when(context.getAttribute(AttributesNames.DAO_FACTORY)).thenReturn(daoFactory);
        when(req.getServletContext()).thenReturn(context);

        userAccessChecker = new UserAccessChecker();
    }

    @Test
    public void preInvokeManagerInvokesAnnotatedMethod() throws Exception {
        preInvoke(new User(0, "", "", "", true), true);
    }

    @Test
    public void preInvokeManagerInvokesNotAnnotatedMethod() throws Exception {
        preInvoke(new User(0, "", "", "", true), false);
    }

    @Test
    public void preInvokeUserInvokesNotAnnotatedMethod() throws Exception {
        preInvoke(new User(0, "", "", "", false), false);
    }

    @Test(expected = AccessException.class)
    public void preInvokeUserInvokesAnnotatedMethod() throws Exception {
        preInvoke(new User(0, "", "", "", false), true);
    }

    @Test(expected = AccessException.class)
    public void preInvokeNullSession() throws Exception {
        when(req.getSession(false)).thenReturn(null);

        userAccessChecker.preInvoke(req, null, null, null);
    }

    @Test(expected = AccessException.class)
    public void preInvokeNullUserIdAttr() throws Exception {
        when(req.getSession(false)).thenReturn(session);
        when(session.getAttribute(AttributesNames.USER_ID)).thenReturn(null);

        userAccessChecker.preInvoke(req, null, null, null);
    }

    private void preInvoke(User user, boolean annotated) throws Exception {
        int id = user.getId();
        when(usersDAO.getUser(id)).thenReturn(user);
        when(req.getSession(false)).thenReturn(session);
        when(session.getAttribute(AttributesNames.USER_ID)).thenReturn(id);

        userAccessChecker.preInvoke(req, null, getMethod(annotated), new Object[0]);
    }

    private class UserAccessCheckerTestHelper {

        @RequirePermission
        public void annotatedMethod() {
        }

        public void notAnnotatedMethod() {
        }
    }

    private Method getMethod(boolean annotated) throws NoSuchMethodException {
        if (annotated)
            return UserAccessCheckerTestHelper.class.getMethod("annotatedMethod");
        else
            return UserAccessCheckerTestHelper.class.getMethod("notAnnotatedMethod");
    }
}