package training.users.dao;

import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import training.users.User;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

import static org.junit.Assert.*;


public class UsersDAOTest {

    private static final String DB_NAME = "usersTestDB";
    private static final String TABLE_NAME = "users";
    private static final String PROPERTY_FILE = "build.properties";
    private static String user;
    private static String pass;
    private static String dbHost;
    private static String port;
    private static Connection connection;

    @BeforeClass
    public static void setUp() throws Exception {
        loadProperties();
        connection = getConnection();
        createTestDatabase();
        createTestTable();
    }

    private static Connection getConnection() throws SQLException {
        String url = "jdbc:mysql://" + dbHost + ":" + port + "/" + "?allowMultiQueries=true";
        return DriverManager.getConnection(url, user, pass);
    }

    private static void loadProperties() throws IOException {
        Properties properties = new Properties();
        ClassLoader loader = Thread.currentThread().getContextClassLoader();
        InputStream is = loader.getResourceAsStream(PROPERTY_FILE);
        if (is == null)
            throw new FileNotFoundException("Property file " + PROPERTY_FILE + " not found");
        properties.load(is);

        user = properties.getProperty("db.user");
        pass = properties.getProperty("db.password");

        dbHost = properties.getProperty("db.host");
        port = properties.getProperty("db.port");
    }

    private static void createTestDatabase() throws SQLException {
        try (Statement statement = connection.createStatement()) {
            statement.executeUpdate("CREATE DATABASE " + DB_NAME);
        }
        connection.setCatalog(DB_NAME);
    }

    private static void dropTestDatabase() throws SQLException {
        try (Statement statement = connection.createStatement()) {
            statement.executeUpdate("DROP DATABASE " + DB_NAME);
        }
    }

    private static void createTestTable() throws SQLException {
        String createTableQuery = "CREATE TABLE " + DB_NAME +
                "." + TABLE_NAME + " " +
                "(" +
                "  id INT NOT NULL AUTO_INCREMENT, " +
                "  user_name VARCHAR(40) UNIQUE NOT NULL, " +
                "  password VARCHAR(40) NOT NULL, " +
                "  first_name VARCHAR(40) NOT NULL, " +
                "  last_name VARCHAR(40) NOT NULL, " +
                "  is_manager BOOLEAN NOT NULL, " +
                "  PRIMARY KEY (id) " +
                ")";
        try (Statement statement = connection.createStatement()) {
            statement.executeUpdate(createTableQuery);
        }
    }

    private static void dropTestTable() throws SQLException {
        String dropQuery = "DROP TABLE IF EXISTS " + DB_NAME + "." + TABLE_NAME;
        try (Statement statement = connection.createStatement()) {
            statement.executeUpdate(dropQuery);
        }
    }

    @Test
    public void getUserCorrectUsernameAndPass() throws SQLException {
        User user = new User(1, "1", "1", "1", true);
        String pass = "1";
        UsersDAO dao = new UsersDAO(connection);
        dao.createUser(user, pass);

        User returnedUser = dao.getUser(user.getUsername(), pass);

        assertEquals(user, returnedUser);
    }

    @Test
    public void getUserCorrectUsernameWrongPass() throws SQLException {
        User user = new User(1, "1", "1", "1", true);
        String pass = "1";
        UsersDAO dao = new UsersDAO(connection);
        dao.createUser(user, "0");

        User returnedUser = dao.getUser(user.getUsername(), pass);

        assertNull(returnedUser);
    }

    @Test
    public void getUserWrongUsernameCorrectPass() throws SQLException {
        User user = new User(1, "1", "1", "1", true);
        String pass = "1";
        UsersDAO dao = new UsersDAO(connection);
        dao.createUser(user, pass);

        User returnedUser = dao.getUser("2", pass);

        assertNull(returnedUser);
    }


    @Test(expected = MySQLIntegrityConstraintViolationException.class)
    public void createUserWithExistingUsername() throws SQLException {
        UsersDAO dao = new UsersDAO(connection);
        dao.createUser(new User(1, "1", "1", "1", true), "1");
        User user = new User(2, "1", "2", "2", false);
        String pass = "2";

        dao.createUser(user, pass);
    }

    @Test
    public void updateUserUpdateAllFields() throws SQLException {
        UsersDAO dao = new UsersDAO(connection);
        String pass = "1";
        dao.createUser(new User(1, "1", "1", "1", true), pass);
        User user = new User(1, "2", "2", "2", false);

        dao.updateUser(user);

        assertEquals(user, dao.getUser(user.getUsername(), pass));
    }

    @Test
    public void updateUserNonExisting() throws SQLException {
        UsersDAO dao = new UsersDAO(connection);
        String pass = "1";
        User user1 = new User(1, "1", "1", "1", true);
        dao.createUser(user1, pass);
        User user2 = new User(2, "2", "2", "2", false);

        dao.updateUser(user2);

        assertEquals(user1, dao.getUser(user1.getUsername(), pass));
    }

    @Test
    public void updateUserToExistingUsername() throws SQLException {
        UsersDAO dao = new UsersDAO(connection);
        String pass = "1";
        User user1 = new User(1, "1", "1", "1", true);
        dao.createUser(user1, pass);
        User user2 = new User(1, "1", "2", "2", false);

        dao.updateUser(user2);
        User updated = dao.getUser(user2.getUsername(), pass);

        assertNotEquals(user1, updated);
        assertEquals(user2, updated);
    }

    @Test
    public void updatePassword() throws SQLException {
        String pass = "1";
        String newPass = "2";
        int id = 1;
        User user = new User(id, "1", "1", "1", true);
        UsersDAO dao = new UsersDAO(connection);
        dao.createUser(user, pass);

        dao.updatePassword(id, newPass);

        assertNull(dao.getUser(user.getUsername(), pass));
        assertEquals(user, dao.getUser(user.getUsername(), newPass));
    }

    @Test
    public void updatePasswordForNonExistingUser() throws SQLException {
        String pass = "1";
        String newPass = "2";
        User user = new User(1, "1", "1", "1", true);
        UsersDAO dao = new UsersDAO(connection);
        dao.createUser(user, pass);

        dao.updatePassword(2, newPass);

        assertNull(dao.getUser(user.getUsername(), newPass));
        assertEquals(user, dao.getUser(user.getUsername(), pass));
    }

    @Test
    public void deleteUserExistingUser() throws SQLException {
        int id = 1;
        String pass = "1";
        User user = new User(id, "1", "1", "1", true);
        UsersDAO dao = new UsersDAO(connection);
        dao.createUser(user, pass);

        dao.deleteUser(id);

        assertNull(dao.getUser(user.getUsername(), pass));
    }

    @Test
    public void deleteUserNonExistingUser() throws SQLException {
        int id = 1;
        String pass = "1";
        User user = new User(id, "1", "1", "1", true);
        UsersDAO dao = new UsersDAO(connection);
        dao.createUser(user, pass);

        dao.deleteUser(2);

        assertEquals(user, dao.getUser(user.getUsername(), pass));
    }

    @Test
    public void getUserExistingUsername() throws SQLException {
        User user = new User(1, "1", "1", "1", false);
        String pass = "1";
        UsersDAO dao = new UsersDAO(connection);
        dao.createUser(user, pass);

        User returnedUser = dao.getUser(user.getUsername());

        assertEquals(user, returnedUser);
    }

    @Test
    public void getUserNonExistingUsername() throws SQLException {
        UsersDAO dao = new UsersDAO(connection);

        User returnedUser = dao.getUser("1");

        assertNull(returnedUser);
    }

    @Test
    public void getUserExistingId() throws SQLException {
        User user = new User(1, "1", "1", "1", false);
        String pass = "1";
        UsersDAO dao = new UsersDAO(connection);
        dao.createUser(user, pass);

        User returnedUser = dao.getUser(user.getId());

        assertEquals(user, returnedUser);
    }

    @Test
    public void getUserNonExistingId() throws SQLException {
        UsersDAO dao = new UsersDAO(connection);

        User returnedUser = dao.getUser(1);

        assertNull(returnedUser);
    }

    @Test
    public void getUsersListFromEmptyDB() throws SQLException {
        UsersDAO dao = new UsersDAO(connection);

        User[] users = dao.getUsersList();

        assertNotNull(users);
        assertEquals(0, users.length);
    }

    @Test
    public void getUsersListFromDBWithOneUser() throws SQLException {
        UsersDAO dao = new UsersDAO(connection);
        User user = new User(1, "1", "1", "1", false);
        dao.createUser(user, "1");

        User[] users = dao.getUsersList();

        assertArrayEquals(new User[]{user}, users);
    }

    @Test
    public void getUsersListFromDBWithSeveralUsers() throws SQLException {
        UsersDAO dao = new UsersDAO(connection);
        int n = 10;
        User[] users = new User[n];
        for (int i = 1; i <= n; i++) {
            User user = new User(i, "" + i, "" + i, "" + i, false);
            dao.createUser(user, "" + i);
            users[i - 1] = user;
        }

        User[] returnedUsers = dao.getUsersList();

        assertArrayEquals(users, returnedUsers);
    }

    @After
    public void clear() throws SQLException {
        try (Statement st = connection.createStatement()) {
            st.executeUpdate("TRUNCATE " + TABLE_NAME);
        }
    }

    @AfterClass
    public static void tearDown() throws Exception {
        dropTestTable();
        dropTestDatabase();
    }
}