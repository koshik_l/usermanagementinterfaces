package training.users.filters;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.Mockito;
import training.users.User;
import training.users.dao.UsersDAO;
import training.users.dao.UsersDaoFactory;
import training.users.utils.AttributesNames;
import training.users.utils.PageNames;

import javax.servlet.FilterChain;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import static org.hamcrest.core.Is.is;
import static org.mockito.Mockito.*;

public class LoginFilterTest {

    private HttpServletRequest req;
    private HttpServletResponse resp;
    private LoginFilter filter;
    private FilterChain chain;
    private UsersDAO usersDAO;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Before
    public void setUp() {
        req = mock(HttpServletRequest.class);
        resp = mock(HttpServletResponse.class);
        chain = mock(FilterChain.class);
        usersDAO = mock(UsersDAO.class);

        filter = new LoginFilter();

        when(req.getContextPath()).thenReturn("contextPath");
        ServletContext context = mock(ServletContext.class);
        UsersDaoFactory daoFactory = mock(UsersDaoFactory.class);
        when(daoFactory.createUsersDAO(Mockito.any(Connection.class))).thenReturn(usersDAO);
        when(context.getAttribute(AttributesNames.DAO_FACTORY)).thenReturn(daoFactory);
        when(req.getServletContext()).thenReturn(context);
    }

    @Test
    public void doFilterLoggedInManager() throws IOException, ServletException, SQLException {
        doFilterLoggedIn(true);
    }

    @Test
    public void doFilterLoggedInUser() throws IOException, ServletException, SQLException {
        doFilterLoggedIn(false);
    }

    @Test
    public void doFilterNullSession() throws IOException, ServletException {
        when(req.getSession(false)).thenReturn(null);

        filter.doFilter(req, resp, chain);

        verify(resp).sendRedirect(PageNames.LOGIN_SERVLET);
        verify(chain, never()).doFilter(req, resp);
    }

    @Test
    public void doFilterNullUserId() throws IOException, ServletException {
        HttpSession session = mock(HttpSession.class);
        when(session.getAttribute(AttributesNames.USER_ID)).thenReturn(null);
        when(req.getSession(false)).thenReturn(session);

        filter.doFilter(req, resp, chain);

        verify(resp).sendRedirect(PageNames.LOGIN_SERVLET);
        verify(chain, never()).doFilter(req, resp);
    }

    @Test
    public void doFilterUsersDaoThrowsException() throws IOException, ServletException, SQLException {
        HttpSession session = mock(HttpSession.class);
        int id = 0;
        when(session.getAttribute(AttributesNames.USER_ID)).thenReturn(id);
        when(req.getSession(false)).thenReturn(session);

        SQLException sqlException = new SQLException();
        doThrow(sqlException).when(usersDAO).getUser(id);
        thrown.expect(ServletException.class);
        thrown.expectCause(is(sqlException));

        filter.doFilter(req, resp, chain);
    }

    private void doFilterLoggedIn(boolean isManager) throws SQLException, IOException, ServletException {
        HttpSession session = mock(HttpSession.class);
        int id = 0;
        when(session.getAttribute(AttributesNames.USER_ID)).thenReturn(id);
        when(req.getSession(false)).thenReturn(session);
        when(usersDAO.getUser(id)).thenReturn(new User(id, "", "", "", isManager));

        filter.doFilter(req, resp, chain);

        verify(resp, never()).sendRedirect(Mockito.anyString());
        verify(req).setAttribute(AttributesNames.IS_MANAGER, isManager);
        verify(chain).doFilter(req, resp);
    }
}