package training.users.filters;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import training.users.JDBCDataSource;
import training.users.utils.AttributesNames;

import javax.servlet.*;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import static org.hamcrest.core.Is.is;
import static org.mockito.Mockito.*;

public class DBConnectionFilterTest {

    private Connection dbConnection;
    private ServletRequest req;
    private ServletResponse resp;
    private FilterChain chain;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Before
    public void setUp() throws Exception {
        dbConnection = mock(Connection.class);
        JDBCDataSource dataSource = mock(JDBCDataSource.class);
        when(dataSource.getConnection()).thenReturn(dbConnection);

        ServletContext context = mock(ServletContext.class);
        when(context.getAttribute(AttributesNames.JDBC_DATA_SOURCE)).thenReturn(dataSource);

        req = mock(ServletRequest.class);
        when(req.getServletContext()).thenReturn(context);


        resp = mock(ServletResponse.class);
        chain = mock(FilterChain.class);
    }

    @Test
    public void doFilter() throws SQLException, ServletException {
        DBConnectionFilter filter = new DBConnectionFilter();

        filter.doFilter(req, resp, chain);
        verify(dbConnection).commit();
        verify(dbConnection).close();
    }

    @Test
    public void doFilterNextInChainThrowsServletException() throws SQLException, IOException, ServletException {
        DBConnectionFilter filter = new DBConnectionFilter();

        ServletException servletException = new ServletException(new SQLException());
        doThrow(servletException).when(chain).doFilter(req, resp);

        thrown.expect(ServletException.class);
        thrown.expectCause(is(servletException));

        try {
            filter.doFilter(req, resp, chain);
        } finally {
            verify(dbConnection).rollback();
            verify(dbConnection).close();
        }
    }
}