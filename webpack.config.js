const path = require('path');
const webpack = require('webpack');

module.exports = {
    target: 'web',
    entry: {
        main: ['./web/js/Main.js'],
        // view: ['./web/js/main.js']
    },
    output: {
        filename : '[name].bundle.js',
        path: path.resolve(__dirname, 'out/artifacts/dist/js'),
        libraryTarget: 'var',
        library: 'Main'
    },
    module: {
        loaders: [
            {
                test: /\.js$/,
                loader: 'babel-loader',
                query: {
                    presets: ['es2015']
                }
            },
            {
                test: /\.css$/,
                use: ['style-loader', 'css-loader']
            },
            {
                test: /\.(eot|svg|ttf|woff|woff2)$/,
                loader: 'file-loader'
            }
        ]
    },

    plugins: [
        new webpack.ProvidePlugin({
            $: "jquery",
            jQuery: "jquery"
        })
    ]
};