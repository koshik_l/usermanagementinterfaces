'use strict';

import User from './User.js';

export default class Client {
    constructor(jsonRpcClient, remoteObjID) {
        this.jsonRPC = jsonRpcClient;
        this.objectID = remoteObjID;
    }

    getUsersList() {
        return this._callRemoteMethod(".getUsersList", []).then(
            response => {
                return this._getUsers(response.result);
            }
        );
    }

    _getUsers(result) {
        return result.map((json) => User.createFromJSON(json));
    }

    _checkResponse(response) {
        if (response.error)
            this._handleError(response.error);
    }

    _callRemoteMethod(method, args) {
        return this.jsonRPC.send(this.objectID + method, args).then(
            response => {
                this._checkResponse(response);
                return response;
            }
        );
    }

    _handleError(error) {
        const CODE_REMOTE_EXCEPTION = 490;
        const knownExceptionsNames = ["training.users.service.UserModificationException",
            "training.users.service.AccessException"];

        if (error.msg && error.code && error.trace) {
            if (error.code === CODE_REMOTE_EXCEPTION &&
                knownExceptionsNames.find(name => error.trace.startsWith(name))) {
                throw new Error(error.msg);
            } else {
                console.error(error.msg + '\n' + error.trace);
            }
        } else {
            console.error(error)
        }
        throw new Error("Failed to perform operation");
    }
}