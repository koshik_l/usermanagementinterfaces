'use strict';

import Factory from "./Factory";
import Client from "./Client";
import Controller from "./controllers/Controller";
import UserView from "./view/UserPageView";

export default class UserFactory extends Factory {

    createClient(jsonRpcClient, remoteObjectId) {
        return new Client(jsonRpcClient, remoteObjectId);
    }

    createController(client, userId) {
        return new Controller(new UserView($('body')), client, userId);
    }
}