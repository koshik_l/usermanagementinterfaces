'use strict';

import ManagerClient from "./ManagerClient";
import ManagerController from "./controllers/ManagerController";
import Factory from "./Factory";
import ManagerView from "./view/ManagerPageView";

export default class ManagerFactory extends Factory {

    createClient(jsonRpcClient, remoteObjectId) {
        return new ManagerClient(jsonRpcClient, remoteObjectId);
    }

    createController(client, userId) {
        return new ManagerController(new ManagerView($('body')), client, userId);
    }
}