'use strict';

import Client from "./Client";

export default class ManagerClient extends Client {
    constructor(jsonRpcClient, remoteObjID) {
        super(jsonRpcClient, remoteObjID);
    }

    newUser(user, pass) {
        return this._callRemoteMethod(".newUser", [user, pass]).then(
            response => {
                return response.result;
            }
        );
    }

    updateUser(user, pass) {
        return this._callRemoteMethod(".updateUser", [user, pass]);
    }

    deleteUser(userId) {
        return this._callRemoteMethod(".deleteUser", [userId]);
    }
}