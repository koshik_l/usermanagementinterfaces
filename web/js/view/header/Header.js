'use strict';

import ToggleButton from "./ToggleButton";
import HeaderButton from "./HeaderButton";
import View from "../View";

export default class Header extends View {

    constructor($container) {
        super($container);
        this.navbarCollapseId = "navbarCollapse";
        this.navBeginClass = "mr-auto";
        this.render();

        new HeaderButton($container.find("." + this.navBeginClass), "User List", "list-btn");
        new ToggleButton($container.find(".navbar"), this.navbarCollapseId);
    }

    _markup() {
        return `` +
            `<nav class="navbar navbar-expand-md navbar-dark bg-dark fixed-top">` +
            `  <div class="collapse navbar-collapse" id="${this.navbarCollapsedId}">` +
            `    <ul class="navbar-nav ${this.navBeginClass}"></ul>` +
            `    <form class="form-inline my-2 my-lg-0" action="logout">` +
            `      <button class="btn btn-dark" type="submit">Logout</button>` +
            `    </form>` +
            `    </ul>` +
            `  </div>` +
            `</nav>`;
    }
}