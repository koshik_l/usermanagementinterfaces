'use strict';

import View from "../View";

export default class ToggleButton extends View {
    constructor($header, navbarCollapseId) {
        super($header);
        this.navbarCollapseId = navbarCollapseId;
        this.render();
    }

    _markup() {
        return `` +
            `<button class="navbar-toggler" type="button" data-toggle="collapse"` +
            `  data-target="#${this.navbarCollapseId}" aria-controls="${this.navbarCollapseId}"` +
            `  aria-expanded="false" aria-label="Toggle navigation">` +
            `    <span class="navbar-toggler-icon"></span>` +
            `</button>`;
    }
}