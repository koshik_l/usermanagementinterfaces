'use strict';

import View from "../View";

export default class HeaderButton extends View {
    constructor($navbarNav, name, id) {
        super($navbarNav);
        this.id = id;
        this.name = name;
        this.render();

        this.button = $navbarNav.find("#" + id)[0];
    }

    _markup() {
        return `` +
            `<li class="nav-item">` +
            `<button class="btn btn-dark" type="submit" id="${this.id}">${this.name}</button>` +
            `</li>`;
    }

    setEvent(event) {
        this.button.onclick = event;
    }
}