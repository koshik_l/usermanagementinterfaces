'use strict';

import Header from "./Header";
import HeaderButton from "./HeaderButton";

export default class ManagerHeader extends Header {
    constructor($container) {
        super($container);
        this.createBtn = new HeaderButton($container.find("." + this.navBeginClass), "Create New User", "create-btn");
    }

    setCreateBtnEvent(handler) {
        this.createBtn.setEvent(handler);
    }
}