'use strict';

export default class View {
    constructor($elem) {
        this.$elem = $elem;
    }

    render() {
        let $content = $(this._markup());
        this.$elem.append($content);
        return $content;
    }

    _markup() {
    }
}