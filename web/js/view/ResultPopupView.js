'use strict';

import View from "./View";

export default class ResultPopupView extends View {

    constructor($body) {
        super($body);
        this.render();
        this.$popup = $('#resultModal');
        this.body = this.$popup.find('.modal-body')[0];
        this.render();
    }

    show() {
        this.$popup.modal('show');
    }

    setText(text) {
        this.body.innerHTML = text;
    }

    _markup() {
        return `` +
            `<div class="modal fade" id="resultModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">` +
            `  <div class="modal-dialog" role="document">` +
            `    <div class="modal-content">` +
            `      <div class="modal-header">` +
            `        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>` +
            `        <button type="button" class="close" data-dismiss="modal" aria-label="Close">` +
            `          <span aria-hidden="true">&times;</span>` +
            `        </button>` +
            `      </div>` +
            `      <div class="modal-body">` +
            `      </div>` +
            `      <div class="modal-footer">` +
            `        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>` +
            `      </div>` +
            `    </div>` +
            `  </div>` +
            `</div>`;
    }
}