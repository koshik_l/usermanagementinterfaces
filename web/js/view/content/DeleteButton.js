'use strict';

import View from "../View";

export default class DeleteButton extends View {

    constructor($row, id, deleteEventHandler) {
        super($row);
        this.id = id;
        this.render();

        this.button = $row.find(`#del-btn-${id}`)[0];
        this.button.onclick = () => deleteEventHandler(id);
    }

    disable() {
        this.button.disabled = true;
    }

    _markup() {
        return `<td><button class="btn" id="del-btn-${this.id}">delete</button> </td>`;
    }
}