'use strict';

import ContentView from "./ContentView";
import UserTableRow from "./UserTableRow";

export default class UserContentView extends ContentView {

    _createRow(user) {
        return new UserTableRow(this.$tbody, user);
    }

    _theadMarkup() {
        return `` +
            `    <tr>` +
            `      <th scope="col">User name</th>` +
            `    </tr>`;
    }
}