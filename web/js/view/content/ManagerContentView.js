'use strict';

import ContentView from "./ContentView";
import ManagerTableRow from "./ManagerTableRow";

export default class ManagerContentView extends ContentView {

    constructor($container) {
        super($container);

        this.updateEventHandler = undefined;
        this.deleteEventHandler = undefined;

        this.rowsMap = {};
    }

    addUser(user) {
        this._createRow(user);
    }

    deleteUser(userId) {
        this.rowsMap[userId].delete();
        delete this.rowsMap[userId];
    }

    updateUser(user) {
        this.rowsMap[user.id].update(user);
    }

    _theadMarkup() {
        return `` +
            `    <tr>` +
            `      <th scope="col">User name</th>` +
            `      <th scope="col">Delete</th>` +
            `    </tr>`;
    }

    _createRow(user) {
        this.rowsMap[user.id] = new ManagerTableRow(this.$tbody, user, this.updateEventHandler, this.deleteEventHandler);
    }

    disableDelete(id) {
        this.rowsMap[id].disableDelete();
    }

    setUpdateBtnEvent(handler) {
        this.updateEventHandler = handler;
    }

    setDeleteHandler(handler) {
        this.deleteEventHandler = handler;
    }
}