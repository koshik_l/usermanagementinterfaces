'use strict';

import View from "../View";

export default class TableRow extends View {

    constructor($tbody, user) {
        super($tbody);
        this.id = user.id;
        this.render();
        this.$row = $tbody.find(".user-" + user.id);
    }

    _markup() {
        return `<tr class="user-${this.id}"></tr>`;
    }

}