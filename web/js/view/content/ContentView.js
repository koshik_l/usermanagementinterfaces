'use strict';

import View from "../View";

export default class ContentView extends View {
    constructor($container) {
        super($container);
        this.render();
        this.$tbody = $container.find("tbody");

        let $thead = $container.find("thead");
        $thead.append(this._theadMarkup());
    }

    _markup() {
        return `` +
            `<table class="table table-bordered">` +
            `  <thead></thead>` +
            `  <tbody></tbody>` +
            `</table>`;
    }

    showUsersList(users) {
        users.map(user => this._createRow(user));
    }

    //abstract
    _createRow(user) {
    }

    _theadMarkup() {
    };
}