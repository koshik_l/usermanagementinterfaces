'use strict';

import View from "../View";

export default class UpdateButton extends View {
    constructor($row, user, updateEventHandler) {
        super($row);
        this.id = user.id;
        this.name = user.firstName + " " + user.lastName + " (" + user.username + ")";
        this.render();

        let button = $row.find(`#upd-btn-${user.id}`)[0];
        button.onclick = () => updateEventHandler(user.id);
        this.button = button;
    }

    _markup() {
        return `<td><button class="btn btn-link" id="upd-btn-${this.id}">${this.name}</button></td>`;
    }

    update(user) {
        this.button.innerHTML = user.firstName + " " + user.lastName + " (" + user.username + ")";
    }
}