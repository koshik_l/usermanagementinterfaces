'use strict';

import TableRow from "./TableRow";

export default class UserTableRow extends TableRow {

    constructor($tbody, user) {
        super($tbody, user);
        this.$row.append(this._rowMarkup(user));
    }

    _rowMarkup(user) {
        return `<td id="user-${user.id}">${user.firstName} ${user.lastName} (${user.username})</td>`;
    }

}