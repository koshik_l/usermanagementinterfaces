'use strict';

import UpdateButton from "./UpdateButton";
import DeleteButton from "./DeleteButton";
import TableRow from "./TableRow";

export default class ManagerTableRow extends TableRow {

    constructor($tbody, user, updateEventHandler, deleteEventHandler) {
        super($tbody, user);
        this.updBtn = new UpdateButton(this.$row, user, updateEventHandler);
        this.delBtn = new DeleteButton(this.$row, user.id, deleteEventHandler);
    }

    disableDelete() {
        this.delBtn.disable();
    }

    update(user) {
        this.updBtn.update(user);
    }

    delete() {
        this.$row.empty();
    }
}