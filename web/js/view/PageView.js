'use strict';

import 'bootstrap'
import 'bootstrap/dist/css/bootstrap.min.css'
import '../../css/view.css'
import $ from 'jquery'
import View from "./View";

export default class PageView extends View {

    constructor($body) {
        super($body);
        this.render();
        let $navbar = $('.navbar');
        this.$navbarContainer = $navbar.find('.container-fluid');
        this.$contentContainer = $('.container');
    }

    _markup() {
        return `` +
            `<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">` +
            `  <div class="container-fluid"></div>` +
            `</div>` +

            `<div class="container"></div>`;
    }

    showUsersList(curUserId, users) {
        this.content.showUsersList(users);
    }
}



