'use strict';

import PageView from "./PageView";
import Header from "./header/Header";
import UserContentView from "./content/UserContentView";

export default class UserView extends PageView {
    constructor($body) {
        super($body);
        this.header = new Header(this.$navbarContainer);
        this.content = new UserContentView(this.$contentContainer);
    }
}