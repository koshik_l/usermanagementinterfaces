'use strict';

import View from "../View";

export default class FormCheckBox extends View {

    constructor($form, name) {
        super($form);
        this.name = name;
        this.render();
        this.input = $form.find('.form-check-input')[0];
    }

    setCheckedState(isChecked) {
        this.input.checked = isChecked;
    }

    isSet() {
        return this.input.checked;
    }

    setDisabledAttribute(disabled) {
        this.input.disabled = disabled;
    }

    _markup() {
        return `` +
            `<div class="check form-group row">` +
            `<div class="offset-sm-3 col-sm-9">` +
            `  <div class="form-check">` +
            `    <input class="form-check-input" type="checkbox">` +
            `    <label class="form-check-label">${this.name}</label>` +
            `  </div>` +
            `</div>` +
            `</div>`;
    }
}