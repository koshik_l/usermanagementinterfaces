'use strict';

import FormRow from "./FormRow";
import FormCheckBox from "./FormCheckBox";
import User from "../../User";
import UserData from "./UserData";
import View from "../View";

export default class PopupForm extends View {

    constructor($form) {
        super($form);

        this.userNameRow = new FormRow($form, "username-row", "Username");
        this.passRow = new FormRow($form, "password-row", "Password", "password");
        this.retypePassRow =
            new FormRow($form, "retype-password-row", "Retype password", "password");
        this.firstNameRow = new FormRow($form, "first-name-row", "First Name");
        this.lastNameRow = new FormRow($form, "last-name-row", "Last Name");
        this.checkBox = new FormCheckBox($form, "Manager");
    }

    getUserData() {
        let username = this.userNameRow.getValue();
        let firstName = this.firstNameRow.getValue();
        let lastName = this.lastNameRow.getValue();
        let isManager = this.checkBox.isSet();
        return new UserData(new User(username, firstName, lastName, isManager),
            this.passRow.getValue(), this.retypePassRow.getValue());
    }

    getPassword() {
        return this.passRow.getValue();
    }

    getRetypedPassword() {
        return this.retypePassRow.getValue();
    }

    setUsernameRowInvalid() {
        this.userNameRow.setInvalid("Provide a valid username");
    }

    setFirstNameRowInvalid() {
        this.firstNameRow.setInvalid("Provide a valid first name");
    }

    setLastNameRowInvalid() {
        this.lastNameRow.setInvalid("Provide a valid last name");
    }

    setPasswordRowInvalid() {
        this.passRow.setInvalid("Provide a valid password");
    }

    setRetypedPasswordRowInvalid() {
        this.retypePassRow.setInvalid("Passwords should be equal");
    }

    clearInvalidStatus() {
        this.userNameRow.clearInvalidStatus();
        this.passRow.clearInvalidStatus();
        this.retypePassRow.clearInvalidStatus();
        this.firstNameRow.clearInvalidStatus();
        this.lastNameRow.clearInvalidStatus();
    }

    setCheckBoxDisableAttr(disabled) {
        this.checkBox.setDisabledAttribute(disabled);
    }

    setValues(user) {
        this.clearInvalidStatus();
        this.userNameRow.setInputText(user.username);
        this.firstNameRow.setInputText(user.firstName);
        this.lastNameRow.setInputText(user.lastName);
        this.checkBox.setCheckedState(user.isManager);

        this.passRow.setInputText("");
        this.retypePassRow.setInputText("");
    }
}