'use strict';

import View from "../View";

export default class SubmitButton extends View {

    constructor($span, name) {
        super($span);
        this.name = name;
        this.render();
        this.button = $span.find(".btn")[0];
    }

    _markup() {
        return `` +
            `<button type="button" class="btn btn-primary">${this.name}</button>`;
    }

    setSubmitHandler(handler) {
        this.button.onclick = handler;
    }
}