'use strict';

import SubmitButton from "./SubmitButton";
import PopupForm from "./PopupForm";
import User from "../../User";
import View from "../View";

export default class PopupView extends View {
    constructor($body, submitBtnName, submitType) {
        super($body);
        this.submitType = submitType;
        this.render();

        this.$popup = $('.modal-container').find(`#${submitType}-modal`);
        this.errorMsg = $body.find(`.error-msg-${this.submitType}`)[0];

        this.popupForm = new PopupForm(this.$popup.find(".popup-form"));
        this.submitButton = new SubmitButton(this.$popup.find(".submit-button-span"), submitBtnName);

        this.$popup.on('hidden.bs.modal', (e) => this.clear());
    }

    show() {
        this.$popup.modal('show');
    }

    hide() {
        this.$popup.modal('hide');
    }

    clear() {
        this.errorMsg.innerHTML = "";
        this.clearValues();
        this.setCheckBoxDisableAttr(false);
    }

    showError(errorMsg) {
        this.errorMsg.innerHTML = errorMsg;
    }

    setValues(user) {
        this.popupForm.setValues(user);
    }

    setCheckBoxDisableAttr(disabled) {
        this.popupForm.setCheckBoxDisableAttr(disabled);
    }

    clearValues() {
        const emptyUser = new User("", "", "", "");
        this.popupForm.setValues(emptyUser);
    }

    getUserData() {
        return this.popupForm.getUserData();
    }

    setSubmitHandler(handler) {
        this.submitButton.setSubmitHandler(() => {
            this.errorMsg.innerHTML = "";
            this.popupForm.clearInvalidStatus();
            handler();
        });
    }

    setUsernameRowInvalid() {
        this.popupForm.setUsernameRowInvalid();
    }

    setFirstNameRowInvalid() {
        this.popupForm.setFirstNameRowInvalid();
    }

    setLastNameRowInvalid() {
        this.popupForm.setLastNameRowInvalid();
    }

    setPasswordRowInvalid() {
        this.popupForm.setPasswordRowInvalid();
    }

    setRetypedPasswordRowInvalid() {
        this.popupForm.setRetypedPasswordRowInvalid();
    }

    _markup() {
        return `` +
            `<div class="modal-container">` +
            `<div class="modal fade" id="${this.submitType}-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">` +
            `  <div class="modal-dialog modal-lg" role="document">` +
            `    <div class="modal-content">` +
            `      <div class="modal-header">` +
            `        <h5 class="modal-title" id="exampleModalLabel">Modal</h5>` +
            `        <button type="button" class="close" data-dismiss="modal" aria-label="Close">` +
            `        <span aria-hidden="true">&times;</span>` +
            `      </button>` +
            `      </div>` +
            `      <div class="modal-body">` +
            `        <div class="error-msg-${this.submitType}"></div>` +
            `        <form class="popup-form needs-validation" novalidate>` +
            `        </form>` +
            `        <div class="modal-footer">` +
            `          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>` +
            `          <span class="submit-button-span"></span>` +
            `        </div>` +
            `      </div>` +
            `    </div>` +
            `  </div>` +
            `</div>` +
            `</div>`;
    }
}
