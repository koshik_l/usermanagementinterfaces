'use strict';

import View from "../View";

export default class FormRow extends View {

    constructor($form, rowId, name, inputType = "text") {
        super($form);
        this.name = name;
        this.inputType = inputType;

        let $row = this.render();
        this.input = $row.find(".form-control")[0];
        this.invalidFeedback = $row.find(".invalid-feedback")[0];
    }

    setInputText(text) {
        this.input.value = (text);
    }

    getValue() {
        return this.input.value;
    }

    setInvalid(errorMsg) {
        this.invalidFeedback.innerHTML = errorMsg;
        this.input.classList.add("is-invalid");
    }

    clearInvalidStatus() {
        this.invalidFeedback.innerHTML = "";
        this.input.classList.remove("is-invalid");
    }

    _markup() {
        return `` +
            `<div class="form-group row">` +
            `  <label class="col-sm-3 col-form-label">${this.name}</label>` +
            `  <div class="col-sm-9">` +
            `    <input type="${this.inputType}" class="form-control">` +
            `    <div class="invalid-feedback"></div>` +
            `  </div>` +
            `</div>`;
    }
}
