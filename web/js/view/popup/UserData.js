'use strict';

export default class UserData {
    constructor(user, pass, retypedPass) {
        this.user = user;
        this.pass = pass;
        this.retypedPass = retypedPass;
    }

    set id(id) {
        this.user.id = id;
    }

    get username() {
        return this.user.username;
    }

    get firstName() {
        return this.user.firstName;
    }

    get lastName() {
        return this.user.lastName;
    }
}