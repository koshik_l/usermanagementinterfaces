'use strict';

import View from "./View";

export default class ConfirmPopupView extends View {
    constructor($body) {
        super($body);
        this.render();

        this.$popup = $body.find(".confirm-modal");
        this.modalBody = this.$popup.find(".modal-body")[0];
        this.deleteButton = this.$popup.find(".delete-btn")[0];
    }

    show() {
        this.$popup.modal("show");
    }

    hide() {
        this.$popup.modal("hide");
    }

    setText(text) {
        this.modalBody.innerHTML = text;
    }

    setOnConfirmHandler(handler) {
        this.deleteButton.onclick = () => {
            this.hide();
            handler();
        };
    }

    _markup() {
        return `` +
            `<div class="modal confirm-modal" tabindex="-1" role="dialog">` +
            `  <div class="modal-dialog" role="document">` +
            `    <div class="modal-content">` +
            `      <div class="modal-header">` +
            `        <h5 class="modal-title">Modal title</h5>` +
            `        <button type="button" class="close" data-dismiss="modal" aria-label="Close">` +
            `          <span aria-hidden="true">&times;</span>` +
            `        </button>` +
            `      </div>` +
            `      <div class="modal-body"></div>` +
            `      <div class="modal-footer">` +
            `        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>` +
            `        <button type="button" class="btn btn-primary delete-btn">Delete</button>` +
            `      </div>` +
            `    </div>` +
            `  </div>` +
            `</div>`;
    }
}