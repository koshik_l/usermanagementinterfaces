'use strict';

import PageView from "./PageView";
import ManagerHeader from "./header/ManagerHeader";
import ManagerContentView from "./content/ManagerContentView";

export default class ManagerPageView extends PageView {

    constructor($body) {
        super($body);
        this.header = new ManagerHeader(this.$navbarContainer);
        this.content = new ManagerContentView(this.$contentContainer);
    }

    showUsersList(curUserId, users) {
        let res = super.showUsersList(curUserId, users);
        this.disableDelete(curUserId);
        return res;
    }

    setCreateBtnEvent(handler) {
        this.header.setCreateBtnEvent(handler);
    }

    addUser(user) {
        this.content.addUser(user);
    }

    updateUser(user) {
        this.content.updateUser(user);
    }

    deleteUser(userId) {
        this.content.deleteUser(userId);
    }

    setUpdateBtnEvent(handler) {
        this.content.setUpdateBtnEvent(handler);
    }

    setDeleteHandler(handler) {
        this.content.setDeleteHandler(handler);
    }

    disableDelete(id) {
        this.content.disableDelete(id);
    }
}

