'use strict';

export default class User {
    constructor(username, firstName, lastName, isManager, id = -1) {
        this.id = id;
        this.username = username;
        this.firstName = firstName;
        this.lastName = lastName;
        this.isManager = isManager;
    }

    static createFromJSON(json) {
        return new User(
            json.username,
            json.firstName,
            json.lastName,
            json.isManager,
            json.id
        );
    }
}