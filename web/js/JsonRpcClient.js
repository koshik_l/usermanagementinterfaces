"use strict";

import $ from 'jquery';

export default class JSONRPCClient {

    constructor (url) {
        this.url = url;
        this.id = 0;
    }

    send(method, args) {
        return new Promise((resolve, reject) => {
            let rpc_request = this._makeRequest(method, args);
            $.post(this.url, JSON.stringify(rpc_request), resolve, "json").fail(
                () => reject(new Error("Failed to call remote method"))
            );
        })
    }

    _makeRequest(methodName, args) {
        let request = {};
        request.id = this.id++;
        request.client = this;
        request.jsonrpc = "2.0";
        request.method = methodName;
        request.params = args;
        return request;
    }

}
