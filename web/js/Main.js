'use strict';

import JSONRPCClient from "./jsonRpcClient";
import ManagerFactory from "./ManagerFactory";
import UserFactory from "./UserFactory";


// $(document).ready(() => {
//     let jsonRPC = new JSONRPCClient("JSON-RPC");
//     let client = new ManagerClient(jsonRPC, "usersService");
//     let controller = new ManagerController(client);
//     controller.getUsersList();
// });

function createFactory(isManager) {
    if (isManager)
        return new ManagerFactory();

    return new UserFactory();
}


module.exports = {
    initClient: function (isManager, remoteObjectId, userId) {
        let jsonRPC = new JSONRPCClient("JSON-RPC");
        let factory = createFactory(isManager);
        let client = factory.createClient(jsonRPC, remoteObjectId);
        let controller = factory.createController(client, userId);
        controller.getUsersList();
    }
};