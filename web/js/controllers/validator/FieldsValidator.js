'use strict';

export default class FieldsValidator {

    validate(userData) {
        let valid = {};
        valid.username = this.notEmpty(userData.username);
        valid.firstName = this.notEmpty(userData.firstName);
        valid.lastName = this.notEmpty(userData.lastName);
        valid.pass = this.isValidPassword(userData.pass);
        valid.retypedPass = this.isValidRetypedPass(userData.pass, userData.retypedPass);
        valid.all = valid.username && valid.firstName && valid.lastName && valid.pass && valid.retypedPass;
        return valid;
    }

    notEmpty(text) {
        return !(!text);
    }

    isValidPassword(pass) {
    }

    isValidRetypedPass(pass, retyped) {
        return pass === retyped;
    }

}