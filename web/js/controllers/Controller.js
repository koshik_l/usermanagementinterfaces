'use strict';

import ResultPopupView from "../view/ResultPopupView";

export default class Controller {
    constructor(view, client, curUserId) {
        this.view = view;
        this.client = client;
        this.usersMap = {};
        this.resultPopupView = new ResultPopupView($('body'));
        this.curUserId = curUserId;
    }

    getUsersList() {
        let users = this.client.getUsersList().then(
            (receivedUsers) => {
                receivedUsers.map(user => this.usersMap[user.id] = user);
                this.view.showUsersList(this.curUserId, receivedUsers);
            })
            .catch(error =>
                this.showResult(error.message));
    }

    addUserToMap(user) {
        this.usersMap[user.id] = user;
    }

    getUserFromMap(id) {
        return this.usersMap[id];
    }

    removeUserFromMap(id) {
        let user = this.usersMap[id];
        delete this.usersMap[id];
        return user;
    }

    showResult(msg) {
        this.resultPopupView.setText(msg);
        this.resultPopupView.show();
    }
}