'use strict';

import PopupController from "./PopupController";
import PopupView from "../view/popup/PopupView";
import UpdateFieldValidator from "./validator/UpdateFieldValidator";

export default class UpdatePopupController extends PopupController {

    constructor($body) {
        let view = new PopupView($body, "Save", "save");
        super(view);
        this.fieldsValidator = new UpdateFieldValidator();
        this.view = view;
        this.curUserId = undefined;
    }

    showPopup() {
        this.view.show();
    }

    disableCheckBox() {
        this.view.setCheckBoxDisableAttr(true);
    }

    setValues(user, loggedInUserId) {
        this.curUserId = user.id;
        this.view.setValues(user);
        if (user.id === loggedInUserId)
            this.disableCheckBox();
    }

    setUpdateHandler(handler) {
        this.view.setSubmitHandler(() => {
            let userData = this.view.getUserData();
            userData.id = this.curUserId;
            let valid = this._validateValues(userData);
            if (!valid)
                return;
            handler(userData.user, userData.pass).then(
                () => this.hidePopup())
                .catch(error => this.showError(error.message));
            // this.hidePopup();
        });
    }
}