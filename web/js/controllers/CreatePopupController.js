'use strict';

import PopupController from "./PopupController";
import PopupView from "../view/popup/PopupView";
import CreateFieldsValidator from "./validator/CreateFieldsValidator";

export default class CreatePopupController extends PopupController {
    constructor($body) {
        let view = new PopupView($body, "Create", "create");
        super(view);
        this.fieldsValidator = new CreateFieldsValidator();
        this.view = view;
    }

    showPopup() {
        this.view.clearValues();
        this.view.show();
    }

    setCreateHandler(handler) {
        this.view.setSubmitHandler(() => {
            let userData = this.view.getUserData();
            let valid = this._validateValues(userData);
            if (!valid)
                return;
            handler(userData.user, userData.pass);
        });
    }


}