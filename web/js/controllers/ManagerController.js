'use strict';

import Controller from "./Controller";
import CreatePopupController from "./CreatePopupController";
import UpdatePopupController from "./UpdatePopupController";
import ConfirmPopupView from "../view/ConfirmPopupView";

export default class ManagerController extends Controller {
    constructor(view, client, curUserId) {
        super(view, client, curUserId);
        let $body = $('body');
        this.createPopupController = new CreatePopupController($body);
        this.updatePopupController = new UpdatePopupController($body);
        this.confirmDeletePopupView = new ConfirmPopupView($body);

        view.setCreateBtnEvent(() => this.createPopupController.showPopup());
        view.setUpdateBtnEvent((userId) => {
            let user = this.getUserFromMap(userId);
            this.updatePopupController.setValues(user, curUserId);
                this.updatePopupController.showPopup();
            }
        );
        view.setDeleteHandler((userId) => this.deleteUser(userId));

        this.createPopupController.setCreateHandler((user, pass) => this.addUser(user, pass));
        this.updatePopupController.setUpdateHandler((user, pass) =>
            this.updateUser(user, pass));
    }

    addUser(user, pass) {
        this.client.newUser(user, pass).then(
            userID => {
                user.id = userID;
                this.addUserToMap(user);
                let username = user.username;
                this.view.addUser(user);
                this.createPopupController.hidePopup();
                this.showResult("User " + username + " was created");
            })
            .catch(error =>
                this.createPopupController.showError(error.message));
    }

    updateUser(user, pass) {
        return this.client.updateUser(user, pass).then(
            () => {
                this.addUserToMap(user);
                let username = user.username;
                this.view.updateUser(user);
                // this.updatePopupController.hidePopup();
                this.showResult("User " + username + " was updated");
            });
        // .catch(error => {
        //     this.updatePopupController.showError(error.message);
        // });
    }

    deleteUser(userId) {
        let username = this.getUserFromMap(userId).username;
        this.confirmDeletePopupView.setOnConfirmHandler(() => {
            this.client.deleteUser(userId).then(
                () => {
                    this.removeUserFromMap(userId);
                    this.view.deleteUser(userId);
                    this.showResult("User " + username + " was deleted");

                })
                .catch(error =>
                    this.showResult(error.message));
        });
        this.confirmDeletePopupView.setText("Are you sure you want to delete user " + username + "?");
        this.confirmDeletePopupView.show();

    }
}