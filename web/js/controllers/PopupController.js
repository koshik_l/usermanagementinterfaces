'use strict';

export default class PopupController {
    constructor(view) {
        this.view = view;
    }

    showPopup(userName, firstName, lastName, isManager) {
        this.view.show(userName, firstName, lastName, isManager);
    }

    hidePopup() {
        this.view.hide();
    }

    showError(errorMsg) {
        this.view.showError(errorMsg);
    }

    _validateValues(userData) {
        let valid = this.fieldsValidator.validate(userData);
        if (!valid.username)
            this.view.setUsernameRowInvalid();
        if (!valid.firstName)
            this.view.setFirstNameRowInvalid();
        if (!valid.lastName)
            this.view.setLastNameRowInvalid();
        if (!valid.pass)
            this.view.setPasswordRowInvalid();
        if (!valid.retypedPass)
            this.view.setRetypedPasswordRowInvalid();
        return valid.all;
    }
}