<%@ page import="training.users.utils.AttributesNames" %>
<%@ page import="training.users.utils.RequestUtils" %>
<%@ page contentType="text/html;charset=UTF-8" %>

<html>
<head>
    <title>Users</title>


</head>
<body>
<script src="js/main.bundle.js"></script>

<%
    int id = RequestUtils.getLoggedInUserId(request);
    boolean isManager = (boolean) request.getAttribute(AttributesNames.IS_MANAGER);
%>
<script>window.onload = (() => Main.initClient(<%= isManager %>, "usersService", <%= id %>))</script>
</body>
</html>
